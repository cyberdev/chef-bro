name 'bro'
maintainer       'Derek Ditch'
maintainer_email 'derek.ditch@gmail.com'
license          'Apache License 2.0'
description      'Installs/Configures bro'
long_description 'Installs/Configures bro'
version          '0.1.0'


depends 'yum'
