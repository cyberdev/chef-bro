
_releaseName = value_for_platform_family(
  ['rhel'] =>"CentOS_#{node['platform_version'].split('.')[0]}",
  ['fedora'] => "Fedora_#{node['platform_version']}"
)

yum_repository 'bro-upstream' do
  description "The Bro Network Security Monitor (#{_releaseName})"
  baseurl 'http://download.opensuse.org/repositories/network:/bro/' + 
    _releaseName
  gpgkey  'http://download.opensuse.org/repositories/network:/bro/' + 
    _releaseName + '/repodata/repomd.xml.key'
  action :create
end

package 'bro'
