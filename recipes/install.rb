
_installRecipe = value_for_platform_family(
  ['debian'] => 'install_apt',
  ['fedora'] => 'install_yum',
  ['rhel']   => 'install_yum'
)

include_recipe "bro::#{_installRecipe}"
